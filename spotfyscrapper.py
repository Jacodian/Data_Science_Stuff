# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 14:03:58 2018

@author: jcarraascootarola
"""
from numpy import genfromtxt
import requests
import spotipy
import spotipy.util as util
import csv
import json
from spotipy.oauth2 import SpotifyClientCredentials
from bs4 import BeautifulSoup
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import urllib

paises = {"Chile":"cl", "Argentina":"ar", "Estados Unidos":"us", "Alemania": "de", "Mexico":"mx" }


#cambiar pais segun se quiera o hacer un for loop si se quieren todos

pais = "Argentina"
codigoPais = paises[pais]


##Preparacion de archivos. modificar los paths segun se quiera (downloads hacia atras)

r=requests.get("http://spotifycharts.com/regional/"+codigoPais +"/weekly/latest/download", allow_redirects=True)

chartPais=open('/Users/jcarraascootarola/Downloads/'+codigoPais +'.csv', 'wb')
chartPais.write(r.content)
chartPais.close()


csvfile = open('/Users/jcarraascootarola/Downloads/'+codigoPais +'.csv', 'r')
jsonfile = open('/Users/jcarraascootarola/Downloads/bandas-'+codigoPais+'.json', 'w')
line = csvfile.readline()


fieldnames = ("Position","Track Name","Artist","Streams","URL")
reader = csv.DictReader( csvfile, fieldnames)

jsonfile.write('{')      
jsonfile.write('\"tracks\"')
jsonfile.write(': [')
for row in reader:
    json.dump(row, jsonfile)
    jsonfile.write(',')
    jsonfile.write('\n')
jsonfile.write("{"+"\"Position\""+":"+ "200}")
jsonfile.write(']}')
jsonfile.close()


data = json.load(open('/Users/jcarraascootarola/Downloads/bandas-'+codigoPais+'.json'))

bandasJson = json.load(open('/Users/jcarraascootarola/Downloads/bandas.json'))




##acceso a la api

client_id = '92de2d5701b94053a2f4e58e07a02588'
client_secret = '7aac7b70ece14132b598472a83adb995'

client_credentials_manager = SpotifyClientCredentials(client_id, client_secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)



#init

bandasLista=[]
for i in bandasJson["bandas"]:
    bandasLista.append(i[:-1])
    

bandasChilenas=[]
bandasInternacionales=[]
generos=[]
features = {'danceability': [], 'energy': [], 'key': [], 'loudness': [], 'mode': [], 'speechiness': [], 'acousticness': [], 'instrumentalness': [], 'liveness': [], 'valence': [], 'tempo': []}


top200 = [[0 for x in range(6)] for y in range(200)] 


#traspasar datos a matriz
  


for i in range(0,200):
    
    top200[i][0]=data["tracks"][i]["Position"]
    top200[i][1]=data["tracks"][i]["Track Name"]
    top200[i][2]=data["tracks"][i]["Artist"]
    top200[i][3]=data["tracks"][i]["Streams"]  
    top200[i][4]=data["tracks"][i]["URL"][31:]
      
    
    birdy_uri = 'spotify:track:' + top200[i][4]  
    track = sp.track(birdy_uri)   
    artist= sp.artist(track['artists'][0]['id'])
    top200[i][5]=artist['popularity']
    
    for genre in artist['genres']:
        generos.append(genre)
    
    print(top200[i][2])
    
    result = sp.audio_features(birdy_uri)[0]
    
    for feature in features.keys():
    
        features[feature].append(result[feature])
     
    
    
    #Destacar chilenos
    artistaABuscar=data["tracks"][i]["Artist"].upper()
    for j in bandasLista:
        
        if  artistaABuscar == j  :
            bandasChilenas.append(artistaABuscar)
            break
    
    if artistaABuscar not in bandasChilenas :
        bandasInternacionales.append(artistaABuscar)
    

#Graficar
labels, values = zip(*Counter(bandasInternacionales).items())

indexes = np.arange(len(labels))
width = 1

plt.bar(indexes, values, width,  ec='black')
plt.xticks(indexes + width * 0.5, labels, rotation='vertical')
plt.title("Artistas internacionales" + pais)
plt.show()


if len(bandasChilenas) > 0:
    labels, values = zip(*Counter(bandasChilenas).items())
    
    indexes = np.arange(len(labels))
    width = 1
    
    plt.bar(indexes, values, width,  ec='black')
    plt.xticks(indexes + width * 0.5, labels, rotation='vertical')
    plt.title("Artistas chilenos "+ pais)
    plt.show()
    
    
    wc = WordCloud(width = 1000, height = 500, background_color ="white").generate(" ".join(bandasChilenas))
    plt.imshow(wc)
    plt.axis('off')
    plt.show()
    


wi = WordCloud(width = 1000, height = 500, background_color ="white").generate(" ".join(bandasInternacionales))
plt.imshow(wi)
plt.axis('off')
plt.show()


labels, values = zip(*Counter(generos).items())
indexes = np.arange(len(labels))
width = 1

plt.bar(indexes, values, width,  ec='black')
plt.xticks(indexes + width * 0.5, labels, rotation='vertical')
plt.title("Generos "+pais)
plt.show()



for feature in features.keys():

    plt.hist(features[feature], bins=10, ec='black')   
    plt.title(feature+" "+pais)
    plt.show()
    
    
    

##guardar Datos en json y generar los graficos de features  

jsonData = open('/Users/jcarraascootarola/Downloads/data-'+codigoPais+'.json', 'w')

jsonData.write('{')      
jsonData.write('\"features\"')
jsonData.write(': {')

for feature in features.keys():

    
    jsonData.write('\"'+feature+'\"')
    jsonData.write(': ')
    jsonData.write(str(features[feature]))
    jsonData.write(',')
    jsonData.write('\n')
    
    
    plt.hist(features[feature], bins=10, ec='black')   
    plt.title(feature+" "+pais)
    plt.show()

jsonData.write('\"Position\"'+":"+ "200")

jsonData.write('}}')
jsonData.close()

    
    

















###No pescar es referencia para webscrapping




            
#page = requests.get("http://www.portaldisc.com/filtro_lista.php")

#soup = BeautifulSoup(page.content, 'html.parser')
#soup2 = soup.find_all('table')
#soup3=soup2[5]



#soup4 = soup3.find_all('tr')
#bandas=[] 
#for i in range(4,len(soup4)-2):
   
   
   
 #  if soup4[i].find_all('td')[3].get_text() == "Chile": 
  #     if soup4[i].find_all('td')[1].get_text() not in bandas:
   #        bandas.append(soup4[i].find_all('td')[1].get_text())
           

  



#birdy_uri = 'spotify:artist:2WX2uTcsvV5OnS0inACecP'
#spotify = spotipy.Spotify()

#results = spotify.artist_albums(birdy_uri, album_type='album')
#print(results)







#grant_type = 'client_credentials'


#body_params = {'grant_type' : grant_type}

#url='https://accounts.spotify.com/api/token'

#response=requests.post(url, data=body_params, auth = (client_id, client_secret)) 


#artist_info = requests.get(
 #   'https://api.spotify.com/v1/search',
  #  headers={ 'access_token': response.json },
  #  params={ 'q': 'daddy', 'type': 'artist' })
        

