#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 12:33:57 2018

@author: jcarraascootarola
"""

from numpy import genfromtxt
import requests
import spotipy
import spotipy.util as util
import csv
import json
from spotipy.oauth2 import SpotifyClientCredentials
from bs4 import BeautifulSoup
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import urllib
import time
from datetime import date
from datetime import timedelta





paises = {"Chile":"cl", "Argentina":"ar", "Estados Unidos":"us", "Alemania": "de", "Mexico":"mx", "Canada":"ca", "España" :"es", "Reino Unido":"gb", "Australia":"au", "Francia":"fr", "Peru":"pe", "Brasil":"br", "Portugal":"pt", "Japon":"jp", "Suiza":"", "Uruguay":"uy", "Colombia":"co", "Paraguay":"py", "Noruega":"no", "Belgica":"be", "Nueva Zelanda":"nz" }                                                                                                  
       

pais = "Nueva Zelanda"
codigoPais = paises[pais]


jsonfile = open('/Users/jcarraascootarola/Desktop/top200/topspais/bandas-'+codigoPais+'.json', 'w')
jsonfile.write('{')      
jsonfile.write('\"tracks\"')
jsonfile.write(': [')

date1 = date(2017,1,27)
date2= date1 + timedelta(days=7)
ultima = date(2018,2,15)
while date1 < ultima:

    fecha = date1.isoformat()+"--"+date2.isoformat()
    print(fecha)
            

    r=requests.get("http://spotifycharts.com/regional/"+codigoPais +"/weekly/"+fecha+"/download", allow_redirects=True)
    
    chartPais=open('/Users/jcarraascootarola/Desktop/top200/'+codigoPais +'-'+str(date1)+'.csv', 'wb')
    chartPais.write(r.content)
    chartPais.close()
    
    
    
    
    csvfile = open('/Users/jcarraascootarola/Desktop/top200/'+codigoPais +'-'+str(date1)+'.csv', 'r')
    
    fieldnames = ("Position","Track Name","Artist","Streams","URL")
    reader = csv.DictReader( csvfile, fieldnames)
    
    
    for row in reader:
        json.dump(row, jsonfile)
        jsonfile.write(',')
        jsonfile.write('\n')
    
    
    date1 = date1 + timedelta(days=7)
    date2= date1 + timedelta(days=7)
    
    
jsonfile.write("{"+"\"Position\""+":"+ "200}")
jsonfile.write(']}')
jsonfile.close()   
csvfile.close()
    
    
    
    
    
    
    
    
    
    